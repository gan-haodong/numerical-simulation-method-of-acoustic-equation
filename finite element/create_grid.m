function GRID = create_grid()
% This function is used to generate  structured and unstructured mesh
GRID = struct('unstructured',@unstructured,...
              'structured',@structured,...
              'connect_mat',@connect_mat);
    function [p,e,t] = unstructured()
        [p, e, t] = initmesh('squareg', 'hmax', 0.08);%单元初始化
        [p,e,t] = refinemesh('squareg',p,e,t);%单元加密
    end
    function [p,e,t] = structured()
        %% 参数设置
            Lx = 1000; %定义单元右边界（左边界为0，如果不是，可以平移为0）
            Ly = 1000;%定义单元上边界
            N = 60;%分割的一个方向的单元数目
            numelx = N;%定义分割的x方向单元数目（按矩形计算）
            numely = N;%定义分割的y方向单元数目（按矩形计算）
            numnodx = numelx + 1; % x方向节点个数比单元个数多1
            numnody = numely + 1; % y方向节点个数比单元个数多1
            nel = 3;%每个单元的节点数目,即每个单元上有几个形函数参与作用，单元自由度
            coordx = linspace(0,Lx,numnodx)'; %等分节点的坐标（为了方便，我这里采用等分的方式，事实上单元长度可以不一致，非均匀网格）
            coordy = linspace(0,Ly,numnody)'; %等分节点的坐标（为了方便，我这里采用等分的方式，事实上单元长度可以不一致）
            [X, Y] = meshgrid(coordx,coordy);%张成网格，X和Y分别表示对应位置的横纵坐标
            X = X';Y = Y';p = [X(:) Y(:)];%把网格一行一行扯开，coord的每一行是对应节点的坐标，按顺序排
            p = p';
            connect = connect_mat(numnodx,numnody,nel);%连接矩阵，表示每个单元周围的节点编号，也就是涉及的形函数编号
            t = connect';
            e = [1:numnodx numnodx*numely+1:numnodx*numely+numnodx ...
                numnodx+1:numnodx:numnodx*(numely-1)+1 2*numnodx:numnodx:numely*numnodx]; % 强制性边界点的编号，本例子中是四条边，下上左右边
    end

    function connect_mat = connect_mat( numnodx,numnody,nel)
        %输入横纵坐标的节点数目，和单元自由度
        %输出连接矩阵，每个单元涉及的节点的编号
        xn = 1:(numnodx*numnody);%拉成一条编号
        A = reshape(xn,numnodx,numnody);%同形状编号
        for i = 1:(numnodx-1)*(numnody-1)
            xg = rem(i,numnodx-1);%xg表示单元为左边界数起第几个
            if xg == 0
                xg = numnodx-1;
            end
            yg = ceil(i/(numnodx-1));%下边界其数第几个
            a = A(xg:xg+1,yg:yg+1);%这个小矩阵，拉直了就是连接矩阵
            a_vec = a(:);
            connect_mat(2*i-1:2*i,1:nel) = [a_vec([1 4 3])';a_vec([4 1 2])'];
        end
    end
end