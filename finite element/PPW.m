%% 等边三角形
clear;clc
tic
%分析PPW对稳定性的影响
q = 0.15;
e = [0,pi/4,pi/2];
y = @(s,e)(1/(pi*q/s).*asin(q/2.*sqrt((24-8*(cos(2*pi/s*cos(e))+2*cos(pi/s*cos(e)).*cos(sqrt(3)*pi/s*sin(e))))...
    /(3+cos(2*pi/s*cos(e))+2*cos(pi/s*cos(e)).*cos(sqrt(3)*pi/s*sin(e))))));
s = 1:1:10;
ns = length(s);
ne = length(e);
Y = zeros(ns,ne);
for i=1:ns
    for j=1:ne
        Y(i,j) = y(s(i),e(j));
    end
    
end
legend_str{ne} = '';
figure(1)
for i=1:ne
    plot(s,Y(:,i),'-*')
    hold on
    legend_str{i} = ['\theta=',num2str(e(i)*180/pi),'°'];
%     pause(0.5)
end
legend(legend_str,'location','best')
title('PPW(equilateral triangle)')
xlabel('ppw')
ylabel('v_h/v')
%% %% 等边三角形传播角度频散现象
clear;clc
%分析传播角度对稳定性的影响
q = 0.15;
e = 0:0.1:pi/2;
y = @(s,e)(1/(pi*q/s).*asin(q/2.*sqrt((24-8*(cos(2*pi/s*cos(e))+2*cos(pi/s*cos(e)).*cos(sqrt(3)*pi/s*sin(e))))...
    /(3+cos(2*pi/s*cos(e))+2*cos(pi/s*cos(e)).*cos(sqrt(3)*pi/s*sin(e))))));
s = 5:1:8;
ne = length(e);
ns = length(s);
Y = zeros(ns,ne);
for i=1:ns
    for j=1:ne
        Y(i,j) = y(s(i),e(j));
    end
    
end
legend_str{ns} = '';
figure(2)
for i=1:ns
    plot(e,Y(i,:),'-*')
    hold on
    legend_str{i} = ['ppw=',num2str(s(i))];
%     pause(0.5)
end
legend(legend_str,'location','best')
title('Angular numerical dispersion(equilateral triangle)')
xlabel('\theta')
set(gca,'xlim',[0,pi/2]);%设置x轴坐标范围
set(gca,'ylim',[1,1.1]);%设置x轴坐标范围
set(gca,'Xtick',0:pi/6:pi/2);
set(gca,'XTickLabel',{'0' '\pi/6' '\pi/3' '\pi/2'});
ylabel('v_h/v')



%% 等腰三角形
clear;clc
%分析PPW对稳定性的影响
q = 0.15;
e = [0,pi/4,pi/2];
y = @(s,e)(1/(pi*q/s).*asin(q.*sqrt((6-3*cos(2*pi/s*cos(e))-3*cos(2*pi/s*sin(e)))...
    /(3+cos(2*pi/s*cos(e))+cos(2*pi/s*sin(e))+cos(sqrt(3)*pi/s*sin(e))+cos(2*pi*cos(e)-2*pi/s*sin(e))))));
s = 1:1:10;
ns = length(s);
ne = length(e);
Y = zeros(ns,ne);
for i=1:ns
    for j=1:ne
        Y(i,j) = y(s(i),e(j));
    end
    
end
figure(3)
legend_str{ne} = '';
for i=1:ne
    plot(s,Y(:,i),'-*')
    hold on
    legend_str{i} = ['\theta=',num2str(e(i)*180/pi),'°'];
%     pause(0.5)
end
legend(legend_str,'location','best')
title('PPW(isosceles triangle)')
xlabel('ppw')
ylabel('v_h/v')


%% %% 等腰三角形传播角度频散现象
clear;clc
%分析传播角度对稳定性的影响
q = 0.15;
e = 0:0.1:pi/2;
y = @(s,e)(1/(pi*q/s).*asin(q.*sqrt((6-3*cos(2*pi/s*cos(e))-3*cos(2*pi/s*sin(e)))...
    /(3+cos(2*pi/s*cos(e))+cos(2*pi/s*sin(e))+cos(sqrt(3)*pi/s*sin(e))+cos(2*pi*cos(e)-2*pi/s*sin(e))))));
s = 5:1:8;
ne = length(e);
ns = length(s);
Y = zeros(ns,ne);
for i=1:ns
    for j=1:ne
        Y(i,j) = y(s(i),e(j));
    end
    end
legend_str{ns} = '';
figure(4)
for i=1:ns
    plot(e,Y(i,:),'-*')
    hold on
    legend_str{i} = ['ppw=',num2str(s(i))];
%     pause(0.5)
end
legend(legend_str,'location','best')
title('Angular numerical dispersion(isosceles triangle)')
xlabel('\theta')
set(gca,'xlim',[0,pi/2]);%设置x轴坐标范围
% set(gca,'ylim',[1,1.3]);%设置x轴坐标范围
set(gca,'Xtick',0:pi/6:pi/2);
set(gca,'XTickLabel',{'0' '\pi/6' '\pi/3' '\pi/2'});
ylabel('v_h/v')

toc

