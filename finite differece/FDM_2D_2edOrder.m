clear;clc;
N = 201;%网格大小
center = 100;%震源放置在中心 
delta_s = 5;%空间剖分间隔大小
delta_t = 0.001;%时间剖分间大小
num_nt = 250;%持续时间或者波场快照结束时间
start_t = 10;%波场快照开始时间
fm = 20;%震源雷克子波中心频率20~40Hz
gama = 4;%控制频带的参数3~5
V(1:N,1:N) = 2500;%波速度
for i=1:50
        V(i,75:125)=2000;
end
%% 初始化网格
w(1:N,1:N) = 0;
w(center,center) = 1;
A(1:N,1:N) = 0;
B(1:N,1:N) = 0;
U(1:N,1:N,1:num_nt) = 0;
%设置震源函数
tt = (1:num_nt)*delta_t;
t_delayed = 0.05; % 子波延时s
temp = pi*fm*(tt-t_delayed);
s_t = (2*temp.^2-1).*exp(-temp.^2);
for i=1:N
    for j=1:N
        A(i,j) = (V(i,j)^2*delta_t^2)/delta_s^2;
        B(i,j) = (V(i,j)*delta_t)/delta_s;
    end
end
for k=2:num_nt-1
    for j=1:N
        for i=2:N-1
            if(j==1)
                %上边界吸收
                U(j,i,k+1) = (2-2*B(j,i)-A(j,i))*U(j,i,k)+2*B(j,i)*(1+B(j,i))*U(j+1,i,k)...
                -A(j,i)*U(j+2,i,k)+(2*B(j,i)-1)*U(j,i,k-1)-2*B(j,i)*U(j+1,i,k-1);
            elseif j==N %下边界吸收
                U(j,i,k+1) = (2-2*B(j,i)-A(j,i))*U(j,i,k)+2*B(j,i)*(1+B(j,i))*U(j-1,i,k)...
                -A(j,i)*U(j-2,i,k)+(2*B(j,i)-1)*U(j,i,k-1)-2*B(j,i)*U(j-1,i,k-1);
            else
                U(j,i,k+1) = s_t(k)*w(j,i)+A(j,i)*(U(j,i+1,k)+U(j,i-1,k)+U(j+1,i,k)+U(j-1,i,k))+...
                    (2-4*B(j,i)^2)*U(j,i,k)-U(j,i,k-1);
            end
        end
    end
    
    for i=1:N
        for j=2:N-1
            if(i==1)
                %左边界吸收
                U(j,i,k+1) = (2-2*B(j,i)-A(j,i))*U(j,i,k)+2*B(j,i)*(1+B(j,i))*U(j,i+1,k)...
                -A(j,i)*U(j,i+2,k)+(2*B(j,i)-1)*U(j,i,k-1)-2*B(j,i)*U(j,i+1,k-1);
            elseif i==N %右边界吸收
                U(j,i,k+1) = (2-2*B(j,i)-A(j,i))*U(j,i,k)+2*B(j,i)*(1+B(j,i))*U(j,i-1,k)...
                -A(j,i)*U(j,i-2,k)+(2*B(j,i)-1)*U(j,i,k-1)-2*B(j,i)*U(j,i-1,k-1);
            else
                U(j,i,k+1) = s_t(k)*w(j,i)+A(j,i)*(U(j,i+1,k)+U(j,i-1,k)+U(j+1,i,k)+U(j-1,i,k))+...
                    (2-4*B(j,i)^2)*U(j,i,k)-U(j,i,k-1);
            end
        end
    end
end
%处理四个角点
for k=1:num_nt
    U(1,1,k) = 1/2*(U(1,2,k)+U(2,1,k));
    U(1,N,k) = 1/2*(U(1,N-1,k)+U(2,N,k));
    U(N,1,k) = 1/2*(U(N-1,1,k)+U(N,2,k));
    U(N,N,k) = 1/2*(U(N-1,N,k)+U(N,N-1,k));
end

%% 绘图
filename = 'FDM_1D_02.gif';
pic_num = 1;
for t=10:10:num_nt
    length = delta_s*(1:N);
    figure(2)
    pcolor(length,length,U(:,:,t))
%     plot(length,U(center,:,t));
%     axis([-inf inf -1 1])
    shading interp;
    str = ['t=',num2str(t),'ms'];
    title(str)
%     if t==20||t==50||t==100||t==150||t==200||t==250
%         saveas(gcf,[str,'_inhomogeneous_model_1.jpg'])
%     end
% %     colormap gray
%     pause(1)
    F=getframe(gcf);
    I=frame2im(F);
    [I,map]=rgb2ind(I,256);
    if pic_num == 1
        imwrite(I,map,filename,'gif','Loopcount',inf,'DelayTime',0.2);
    else
        imwrite(I,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
    pic_num = pic_num + 1;
    pause(0.1);
end