clear;clc
t_pre = clock;
t_start = clock;
%% 生成单元
% [p, e, t] = initmesh('squareg', 'hmax', 0.08);%单元初始化
% [p,e,t] = refinemesh('squareg',p,e,t);%单元加密
GRID = create_grid();
[p,e,t] = GRID.unstructured();
expand = 1000;
p = expand*p;
pdemesh(p, e, t);%'NodeLabels','on'——可以显示节点编号，'ElementLabels','on'——显示单元编号

%生成结构化三角网格
% GRID = create_grid();
% [p,e,t] = GRID.structured();


num_nodes = length(p);
num_elements = length(t);
t_s = sprintf('nodes=%d  elements=%d',num_nodes,num_elements);
title(t_s)
% 在点矩阵p中，第一行和第二行包含网格中点的x和y坐标。
% 在边矩阵e中，第一行和第二行包含起始点和结束点的索引，第三和第四行包含起始和结束参数值，第五行包含边缘段编号，第六和第七行包含左侧和右侧子域编号。
% 在三角形矩阵t中，前三行包含角点的索引，按逆时针顺序给出，第四行包含子域编号。
%% 参数设置
nt = 150; dt = 0.003;
v = 1500; 
T = (1:nt)*dt;
number_of_notes = length(p);
fmain = 40;
%% 设置震源
s_t = (1-2*pi^2*fmain*(T-0.2).^2).*exp(-fmain*pi^2*(T-0.2).^2);%雷克子波
% a = 1;b = 0.04;c = 0.005;
% s_t = a*exp(-(T-b).^2/(2*c^2));%高斯震源
% plot(T,s_t)
%% 调用函数计算矩阵
FEM = FEM_2D_func();
S = FEM.assemble_matrix_2D(p, t);%刚度矩阵
[M,F]= FEM.assemble_vector_2D(p, t);%质量矩阵和右端向量
boundarynodes = unique([e(1, :) e(2, :)]);
% boundarynodes = e;
U = zeros(number_of_notes,nt);
[m,source_x] = find(abs(p(1,:))>=0&abs(p(1,:))<=50&abs(p(2,:))>=0&abs(p(2,:))<=50);

%% 利用递推关系求波场值
for i = 2:nt-1
    U(source_x(1),i) = s_t(i);
%     U(1922,i) = s_t(i);
    %隐式
%     U(:,i+1) = (M+v^2*dt^2*S)\(M*(2*U(:,i)-U(:,i-1)));
%     %显式
    right_vector = v^2*dt^2*S*U(:,i);
    U(:,i+1) = 2*U(:,i)-U(:,i-1)-M\right_vector;
    
    U(boundarynodes,i+1) = 0;
    if(mod(i,10)==0)
        fprintf('time step=%d total=%ds\n',i,nt);
    end
end
fprintf('time step=%d total=%ds\n',nt,nt);
total_time = etime(clock,t_start)/60;
fprintf('total runtime %.2fminutes',total_time)
%% 绘图

filename = sprintf('FEM-2D dt=%.3f fm=%.1f v=%d explicit.gif',dt,fmain,v);
pic_num = 1;
for i=5:5:nt
    trisurf(t(1: 3, :)', p(1, :)', p(2, :)', U(:,i))
    str = sprintf('time step=%d\ndt=%.3f fm=%.1f v=%d',i,dt,fmain,v);
    title(str)
    colorbar
    shading interp
    view([90,90])
    F = getframe(gcf);
    I = frame2im(F,256);
    [I,map]=rgb2ind(I,256);
    if pic_num == 1
        imwrite(I,map,filename,'gif','Loopcount',inf,'DelayTime',0.2);
    else
        imwrite(I,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
    pic_num = pic_num + 1;
end
%% 获取波场快照
trisurf(t(1: 3, :)', p(1, :)', p(2, :)', U(:,140))
str = sprintf('time step=%d\ndt=%.3f fm=%.1f v=%d',140,dt,fmain,v);
title(str)
shading interp
view([90,90])