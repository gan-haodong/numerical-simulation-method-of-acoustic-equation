clear;clc;
%% 设置半圆形极坐标模型参数
R = 1000;
nr = 200;
ne = 31;
nt = 500;
dr = R/nr;
de = pi/ne;
dt = 0.001;
fmain = 20;
v = 2500;
a = (dt*nr/R)^2;
epoch = 10;
num_save = nt/epoch;
count = 1;
%% 初始化变量
U_past = zeros(nr,ne);
U_now = zeros(nr,ne);
U_next = zeros(nr,ne);
U_save = zeros(nr,ne,num_save);
rho = zeros(nr,ne);
theta = zeros(nr,ne);
r = (1:nr)*dr;
e = linspace(-pi,0,ne);
t = (1:nt)*dt;
%震源
ft = (1-2*(pi*fmain*(t-0.05)).^2).*exp(-(pi*fmain*(t-0.05)).^2);
for i=1:ne
    for j=1:nr
        rho(j,i) = r(j);
        theta(j,i) = e(i);
    end
end
[X, Y] = pol2cart(theta, rho);
% polar(theta,rho,'.')
% title('Distribution of points in polar coordinates')
% saveas(gcf, 'polar_points_distribution', 'png')
%% 正演计算
for it=1:nt
    
    for II=2:nr-1
        for JJ=2:ne-1
            %圆心处震源
            U_next(1,JJ) = 2*a*v^2*U_now(2,JJ)+(2*2*a*v^2)*U_now(1,JJ)-U_past(1,JJ)+dt^2*v^2*ft(it);
            
            U_next(II,JJ) = a*v^2*(1+1./(2*II)).*U_now(II+1,JJ)+(2-2*a*v^2-2*a*v^2*ne^2./(pi^2*II.^2)).*U_now(II,JJ)...
                +(1-1./(2*II)).*a*v^2*U_now(II-1,JJ)+a*ne^2*v^2./(pi^2*II.^2).*(U_now(II,JJ+1)+U_now(II,JJ-1))-U_past(II,JJ);
            
            
        end
    end
    %边界条件
    U_next(nr,1:ne) = U_next(nr-1,1:ne);
    U_next(1:nr,1) = U_next(1:nr,2);
    U_next(1:nr,ne) = U_next(1:nr,ne-1);
    if(mod(it,10)==0)
        fprintf('time step=%d/%d\n',it,nt)
        U_save(:,:,count) = U_next;
        count = count + 1;
%         h = polar(theta,rho); %产生在极坐标下的一条直线
%         delete(h);   %删除上述直线，但留下坐标轴
%         hold on
%         pcolor(X,Y,U_next);
%         xlabel('X')
%         ylabel('Z')
%         shading interp
%         hold off
%         pause(0.1)
    end
    U_past = U_now;
    U_now = U_next;
end
%% 绘制动图并保存
%% 绘制并保存动图
pic_num = 1;
filename = 'FDM-2D in hemispherical.gif';
for i=1:num_save
%     h = polar(theta,rho); %产生在极坐标下的一条直线
%     delete(h);   %删除上述直线，但留下坐标轴
%     hold on
    figure(1)
    pcolor(X,Y,U_save(:,:,i));
    shading interp
%     colorbar
    hold off
    str = sprintf('FDM-2D in hemispherical\ntime=%dms',i*epoch);
    title(str)
    set(gca,'xticklabel',[])
    set(gca,'yticklabel',[])
    set(gca,'xtick',[])
    set(gca,'ytick',[])

    axis tight;
    axis equal
%     str = sprintf('hemispherical-2D-time=%dms.png',i*epoch);
%     if(mod(i,10)==0)
%         saveas(gcf, str, 'png')
%     end
    F = getframe(gcf);
    I = frame2im(F);
    [I,map]=rgb2ind(I,256);
    if pic_num == 1
        imwrite(I,map,filename,'gif','Loopcount',inf,'DelayTime',0.2);
    else
        imwrite(I,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
    pic_num = pic_num + 1;
    pause(0.1);
end

%% 保存静态图像
figure(2)
pcolor(X,Y,U_save(:,:,30))
shading interp
axis tight;
axis equal
str = sprintf('FDM-2D in hemispherical\ntime=%dms',30*epoch);
title(str)
set(gca,'xtick',[],'xticklabel',[])
set(gca,'ytick',[],'yticklabel',[])
saveas(gcf, filename, 'png')

