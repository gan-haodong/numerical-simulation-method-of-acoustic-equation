clear;clc
tic
nx = 1000;nt = 1000;
dx = 5;  dt = 0.001;
fmain = 40; v = 1500;
source_position = floor(nx/2);
%% source
T = (1:nt)*dt;
X = (1:nx)*dx;
s_t = (1-2*pi^2*fmain*(T-0.15).^2).*exp(-pi^2*fmain*(T-0.15).^2);
w = zeros(1,nx);
w(1,source_position) = 1;
plot(s_t)
A = (v*dt/dx);
U = zeros(nx,nt);
%% 使用循环计算
%初始条件和边界
U(1,:) = 0;
U(nx,:) = 0;
for i=2:nt-1
    U(source_position,i) = s_t(i);
    %边界    
    U(1,i+1) = (2-2*A-A^2)*U(1,i)+2*A*(1+A)*U(2,i)-A^2*U(3,i)+(2*A-1)*U(1,i-1)-2*A*U(2,i-1);
    U(nx,i+1) = (2-2*A-A^2)*U(nx,i)+2*A*(1+A)*U(nx-1,i)-A^2*U(nx-2,i)+(2*A-1)*U(nx,i-1)-2*A*U(nx-1,i-1);
    for j = 3:nx-2
        %递推
        U(j,i+1) = w(j)*s_t(i)+2*U(j,i)-U(j,i-1)+A^2*(-1/12*(U(j-2,i)+U(j+2,i))+4/3*(U(j+1,i)+U(j-1,i))-2.5*U(j,i));
    end
    if(mod(i,100)==0)
            fprintf('step=%d,total=%d\n',i,nt);
    end
end
%% 绘图
pic_num = 1;
filename = sprintf('FDM_1D_fmain=%d.gif',fmain);
for i=10:10:nt
    plot(X,U(:,i))
    str = sprintf('FDM-1D\ntime step=%d\ndx=%d dt=%.3f v=%d fmain=%d',i,dx,dt,v,fmain);
    title(str)
    axis([-inf inf -1 1])

    F = getframe(gcf);
    I = frame2im(F);
    [I,map]=rgb2ind(I,256);
    if pic_num == 1
        imwrite(I,map,filename,'gif','Loopcount',inf,'DelayTime',0.2);
    else
        imwrite(I,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
    pic_num = pic_num + 1;
end
%% 静态图像
figure(2)
plot(X,U(:,nt))
str = sprintf('FDM-1D\ntime step=%d\ndx=%d dt=%.3f v=%d fmain=%d',nt,dx,dt,v,fmain);
title(str)
axis([-inf inf -1 1])
toc
    