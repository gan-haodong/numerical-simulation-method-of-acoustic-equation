clear;clc
%% 设置模型参数
dx = 10;dt = 0.001; 
nx = 500; nt = 1000;
v = 1500; fmain = 30;
%% 初始化变量
x = (1:nx)*dx;
t = (1:nt)*dt;
u = zeros(nx,nt);
%% 设置震源
s_t = (1-2*pi^2*fmain*(t-0.15).^2).*exp(-fmain*pi^2*(t-0.15).^2);%雷克子波
% a = 10;b = 0.05;c = 0.01;
% s_t = a*exp(-(t-b).^2/(2*c^2));%高斯震源
% figure(1)
% plot(t,s_t)
% title('source')
% xlabel('t')

%% 设置边界及初始条件
u(1,:) = 0;
u(nx,:) = 0;
u(floor(nx/2),:) = s_t;
%% FEM求解
stiffness_matrix = zeros(nx-2,nx-2);%刚度矩阵
mass_matrix = zeros(nx-2,nx-2);%质量矩阵
F = zeros(nx-2,1);
for i=1:nx-2
    if i>1
        stiffness_matrix(i-1,i) = -1/dx;
        mass_matrix(i-1,i) = dx/6;
        stiffness_matrix(i,i-1) = -1/dx;
        mass_matrix(i,i-1) = dx/6;
        
    end
    stiffness_matrix(i,i) = 2/dx;
    mass_matrix(i,i) = 2/3*dx;
    
    
end
for II = 2:nt-1
%     F(floor(nx/2),1) = s_t(II);
    u(floor(nx/2),II) = s_t(II);
    u(1,II) = 0;
    u(nx,II) = 0;
    %隐式
%     load_vector =  mass_matrix * (2*u(2:end-1,II)-u(2:end-1,II-1));
%     u(2:end-1,II+1) =  ( dt^2*v^2 * stiffness_matrix + mass_matrix ) \ ( load_vector );
%     %显式
    right_vector = v^2*dt^2*stiffness_matrix*u(2:end-1,II);
    u(2:end-1,II+1) = 2*u(2:end-1,II)-u(2:end-1,II-1)-mass_matrix\right_vector;

    if(mod(II,100)==0)
        fprintf('step=%d,total=%d\n',II,nt);
    end
end
%% 图形绘制
figure(2)
n = size(u,2);
num_slice = size(10:10:n,1);
fmat=moviein(num_slice);
filename =sprintf('FEM_1D_fmain=%d.gif',fmain);
pic_num = 1;
for i=10:10:n
    plot(x,u(:,i))
    axis([-inf inf -1 1])
    grid on
    str = sprintf('FEM-1D\ntime step=%d\ndt=%.3f dx=%d fmain=%.1f',i,dt,dx,fmain);
    title(str)
    fmat(:,i)=getframe;
    f=getframe(gcf);
    I=frame2im(f);
    [I,map]=rgb2ind(I,256);
    if pic_num == 1
        imwrite(I,map,filename,'gif','Loopcount',inf,'DelayTime',0.2);
    else
        imwrite(I,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
    pic_num = pic_num + 1;
end
%% 静态图像
figure(2)
plot(x,u(:,nt))
str = sprintf('FEM-1D\ntime step=%d\ndx=%d dt=%.3f v=%d fm=%d',nt,dx,dt,v,fmain);
title(str)
