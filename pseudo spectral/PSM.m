clear
tic
%% 读取高程数据
fid = fopen('bathy.out.linux','rb');
a = fread(fid,[1000,800],'float32');
a=fliplr(a);
a = a';
fclose(fid);
%% 参数设置
g = 9.8; % gravity acceleration(m/s^2)
V = real(sqrt(g*(-a)));%velocity(m/s)
delta_t = 10; % s
delta_s = 10000; % 空间差分：delta_s = delta_x = delta_y (m)
nx = 1000;
ny = 800;
nt = 3000;
fmain = 0.002;
slice = 100;%每间隔100步保存结果
slice_num = nt/slice;
%% 初始化
%% 震源
t0 = 1/(fmain*delta_t);
II = (1:nt)*delta_t;
tmp = (fmain*pi*(II-1/fmain)).^2;
s_t = (1-2*tmp).*exp(-tmp)*delta_t^2;

%变量初始化
P_current = zeros(ny,nx);
P_next = zeros(ny,nx);
P_past = zeros(ny,nx);
Px_fft = complex(zeros(nx,ny));
Py_fft = complex(zeros(ny,nx));
Px_ifft = complex(zeros(nx,ny));
Py_ifft = complex(zeros(ny,nx));
P_psm = zeros(ny,nx);
P_slice =zeros(ny,nx,slice_num);
slice_count = 1;
%% 计算波数K值
kmax = pi/delta_s;
delta_kx = kmax/(nx/2);
delta_ky = kmax/(ny/2);
kx = zeros(nx,1);
ky = zeros(ny,1);
II = 1:1:nx/2;
JJ = 1:1:ny/2;
kx(II,1) = II*delta_kx;
kx(nx/2+II,1) = -kmax + II*delta_kx;
ky(JJ,1) = JJ*delta_ky;
ky(ny/2+JJ,1) = -kmax + JJ*delta_ky;
%计算需要使用的几个系数
coefficient_V = V.^2*delta_t^2;
coefficient_kx = -kx.^2;
coefficient_ky = -ky.^2;
A = V.^2*delta_t^2/delta_s^2;
B = V*delta_t/delta_s;
%% 开始计算
start_time = clock;
for T = 1:nt
    %加源（9点）
    P_current(350,700) = P_current(350,700) + delta_t^2*s_t(1,T);
    %傅里叶变换
    Px_fft = fft(P_current');
    Py_fft = fft(P_current);
    II = 1:ny;
    Px_fft(:,II) = coefficient_kx.*Px_fft(:,II);
    II = 1:nx;
    Py_fft(:,II) = coefficient_ky.*Py_fft(:,II);
    %反傅里叶变换
    Px_ifft = ifft(Px_fft)';
    Py_ifft = ifft(Py_fft);
    P_psm = real(Px_ifft+Py_ifft);
    %递推公式计算
    P_next = 2*P_current-P_past+coefficient_V.*P_psm;
    P_past = P_current;
    P_current = P_next;
    if(mod(T,slice)==0)
        run_time = etime(clock,start_time);
        fprintf('step=%d,total=%d,累计耗时%.2fs\n',T,nt,run_time);
        P_slice(:,:,slice_count) = P_next;
        slice_count = slice_count + 1;
    end
end
%% 绘图
P_slice(abs(P_slice)>10^3) = P_slice(abs(P_slice)>10^3)/10^2;
P_slice(P_slice~=0 & P_slice>10) = P_slice(P_slice~=0 & P_slice>10) + 6000;
P_slice(P_slice~=0 & P_slice<-10) = P_slice(P_slice~=0 & P_slice<-10)-6000;
%% 制作动图
fmat=moviein(30);
filename = 'PSM.gif';
for II = 1:30
    imagesc(P_slice(:,:,II)+a);
    shading interp;
    axis tight;
    set(gca,'yDir','reverse');
    str_title = ['PSM  t=',num2str(delta_t*II*100),'s'];
    title(str_title)
    drawnow; %刷新屏幕
    F = getframe(gcf);%捕获图窗作为影片帧
    I = frame2im(F); %返回图像数据
    [I, map] = rgb2ind(I, 256); %将rgb转换成索引图像
    if II == 1
        imwrite(I,map, filename,'gif', 'Loopcount',inf,'DelayTime',0.1);
    else
        imwrite(I,map, filename,'gif','WriteMode','append','DelayTime',0.1);
    end
    fmat(:,II)=getframe;
end
movie(fmat,10,5);
%% 保存图片为jpg

for II = 5:5:30
    h = figure(2);
    pcolor(P_slice(:,:,II)+a)
    shading interp;
    axis tight;
    set(gca,'yDir','reverse');
    str_title = ['PSM-homogenerous  t=',num2str(delta_t*II*100),'s'];
    title(str_title)
    set(h,'visible','off'); 
    filename = [str_title,'.jpg'];
    saveas(gcf,filename)
    close(gcf)
end
%% 耗时
toc





