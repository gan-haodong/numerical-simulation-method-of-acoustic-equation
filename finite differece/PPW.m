%% FFD-2-PPW
clear
clear 
clc
x = 1:20;
e = 0.1:0.2:0.9;
y(1:20,1:5) = 0;
t = pi/6;
c = [-1,0.5];
for j=1:5
    for i=1:20
        a = 0;
        for k = 1:2
            a = a + c(k)*(sin(pi*cos(t)*(k-1)/x(i))^2+sin(pi*sin(t)*(k-1)/x(i))^2);
        end
        y(i,j) = x(i)/(pi*e(j))*asin(e(j)*sqrt(a));
    end
end
figure(1)
plot(x,y(:,1),x,y(:,2),x,y(:,3),x,y(:,4),x,y(:,5),'linewidth',1.5)
hold on
legend('\epsilon=0.1','\epsilon=0.3','\epsilon=0.5','\epsilon=0.7','\epsilon=0.9','fontsize',15,'location','best')
title('FDM-2 dispersion curve(\theta=\pi/6)')
xlabel('PPW')
ylabel('V_p/V')

%% FFD-5-PPW
clear
clc
x = 1:20;
e = 0.1:0.2:0.7;
y(1:20,1:4) = 0;
t = pi/2;
c = [-5/2,4/3,-1/12];
for j=1:4
    for i=1:20
        a = 0;
        for k = 1:3
            a = a + c(k)*(sin(pi*cos(t)*(k-1)/x(i))^2+sin(pi*sin(t)*(k-1)/x(i))^2);
        end
        y(i,j) = x(i)/(pi*e(j))*asin(e(j)*sqrt(a));
    end
end
figure(2)
plot(x,y(:,1),x,y(:,2),x,y(:,3),x,y(:,4),'linewidth',1.5)
hold on
legend('\epsilon=0.1','\epsilon=0.3','\epsilon=0.5','\epsilon=0.7','fontsize',15,'location','best')
title('FDM-4 dispersion curve(\theta=\pi/6)')
xlabel('PPW')
ylabel('V_p/V')
%% PSM-PPW
clear 
clc
x = 1:20;
e = 0.1:0.1:0.3;
y(1:20,1:3) = 0;
for j=1:3
    for i=1:20
        y(i,j) = x(i)/(pi*e(j))*asin(pi*e(j)/x(i));
    end
    
end
figure(3)
plot(x,y(:,1),x,y(:,2),x,y(:,3),'linewidth',1.5)
hold on
legend('\epsilon=0.1','\epsilon=0.2','\epsilon=0.3','fontsize',15)
title('PSM dispersion curve')
xlabel('PPW')
ylabel('V_p/V')

%% FFD-2-PPW-theta
clear
clc
x = linspace(0,pi,100);
ppw = [5,10,15];
y(1:20,1:5) = 0;
c = [-1,0.5];
for j=1:3
    for i=1:100
        a = 0;
        for k = 1:2
            a = a + c(k)*(sin(pi*cos(x(i))*(k-1)/ppw(j))^2+sin(pi*sin(x(i))*(k-1)/ppw(j))^2);
        end
        y(i,j) = ppw(j)/(pi*0.3)*asin(0.3*sqrt(a));
    end
end
figure(4)
plot(x,y(:,1),x,y(:,2),x,y(:,3),'linewidth',1.5)
hold on
legend('PPW=5','PPW=10','PPW=15','fontsize',10,'location','best')
title('FDM-2 dispersion curve(\epsilon=0.3)')
set(gca,'XTick',0:pi/2:pi);
set(gca,'XTickLabel',{'0','\pi/2','\pi'})
xlabel('\theta')
ylabel('V_p/V')
