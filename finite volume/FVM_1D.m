clear;clc;
%% 模型参数
tic
nx = 800;%单元数
nt = 900;%时间步数
c = 2500;%速度
density = 2500;%密度
shear_modulu = c^2*density;%shear modulus
dt = 0.002;
dx = 10;
e = 200;%(Gauss)
x0 = dx*nx/2;%source position
X = (1:nx)*dx;
fmain = 10;
%% 初始化变量
Q = zeros(2,nx);
Qnew = zeros(2,nx);
% Qnew(1,:) =  exp(-1/e ^ 2 * (X - x0) .^ 2);%source
% Qnew(2,:) =  exp(-1/e ^ 2 * (X - x0) .^ 2);%source
A = [0,-shear_modulu;-1/density,0];
Q_stress = zeros(nx,nt);
Q_velocity = zeros(nx,nt);
t = ((1:nt)-30)*dt;
ft = (2*(pi*fmain*t).^2-1).*exp(-(pi*fmain*t).^2);
%初始条件
for i=1:nt
    Qnew(1,nx/2) = ft(i);
    Qnew(2,nx/2) = ft(i);
    Q = Qnew;
    for j=2:nx-1
        dQ1 = Q(:,j+1)-Q(:,j-1);
        dQ2 = Q(:,j-1)-2*Q(:,j)+Q(:,j+1);
        Qnew(:,j) = Q(:,j) - dt/(2*dx)*A*dQ1+dt^2/(2*dx^2)*(A*A)*dQ2;
    end
    %吸收边界
    Qnew(:,1) = Qnew(:,2);
    Qnew(:,nx) = Qnew(:,nx-1);
    if(mod(i,100)==0)
        fprintf('time step=%d total=%d\n',i,nt)
%         plot(X,Qnew(1,:))
%         str = sprintf('time step=%d',i);
%         title(str)
%         pause(0.1)
    end
    Q_stress(:,i) = Qnew(1,:);
    Q_velocity(:,i) = Qnew(2,:);
    
end
%% 绘图
pic_num = 1;
filename = sprintf('FVM_1D_fmain=%d.gif',fmain);
for i=10:10:nt
    subplot(211)
    plot(X,Q_stress(:,i)/10^6)
    str = sprintf('FVM-1D-stress\ntime step=%d',i);
    xlabel('X');
    ylabel('Stress','FontWeight','bold');
    title(str)
%     axis([-inf inf -4 4])
    subplot(212)
    plot(X,Q_velocity(:,i))
    str = sprintf('FVM-1D-velocity\ntime step=%d',i);
    title(str)
    xlabel('X');
    ylabel('Velocity','FontWeight','bold');
%     axis([-inf inf 0 0.8])

    F = getframe(gcf);
    I = frame2im(F);
    [I,map]=rgb2ind(I,256);
    if pic_num == 1
        imwrite(I,map,filename,'gif','Loopcount',inf,'DelayTime',0.2);
    else
        imwrite(I,map,filename,'gif','WriteMode','append','DelayTime',0.2);
    end
    pic_num = pic_num + 1;
end
%% 静态图像
figure(2)
subplot(211)
plot(X,Q_stress(:,500),'linewidth',1.5)
str = sprintf('FVM-1D-stress\ntime step=%d',500);
xlabel('X');
ylabel('Stress','FontWeight','bold');
title(str)
subplot(212)
plot(X,Q_velocity(:,500),'linewidth',1.5)
str = sprintf('FVM-1D-velocity\ntime step=%d',500);
title(str)
xlabel('X');
ylabel('Velocity','FontWeight','bold');
% axis([-inf inf -1 1])
toc
